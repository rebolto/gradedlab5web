"use strict";
/**
 * @author Noah Diplarakis
 * @version 2023-11-16
 * @description This javascript is used to fetch and display images from the NASA EPIC API based on the user inputs.
 */
!(function () {
  
  const EpicApplication = {
    imageTypes: ["natural", "enhanced", "aerosol", "cloud"],
    listOfImages: [],
    typeInput: null,
    dateCache: {},
    imageCache: {},
  };
  EpicApplication.imageTypes.forEach((element) => {
    EpicApplication.dateCache[element] = null;
    EpicApplication.imageCache[element] = new Map();
  });
  document.addEventListener("DOMContentLoaded", function (e) {
    let liTemplate = document.getElementById("image-menu-item");
    let ulmenu = document.getElementById("image-menu");
    let earthImage = document.getElementById("earth-image");
    let earthImageDate = document.getElementById("earth-image-date");
    let earthImageTitle = document.getElementById("earth-image-title");
    let typeInput = document.getElementById("type");
    let dateInput = document.getElementById("date");

    generateTypeOptions(typeInput, EpicApplication.imageTypes);

    setMaxDate(ulmenu, dateInput, typeInput);

    document
      .querySelector("#request_form")
      .addEventListener("submit", function (e) {
        e.preventDefault();
        ulmenu.textContent = undefined;
        EpicApplication.listOfImages = [];
        let typeOfImg = document.getElementById("type").value;
        let dateOfImg = document.getElementById("date").value;
        liTemplate = document.getElementById("image-menu-item");
        if (EpicApplication.imageCache[typeOfImg].has(dateOfImg)) {
          displayCachedImages(typeOfImg, dateOfImg, ulmenu, liTemplate);
        } 
        
        else {
          fetch(
            `https://epic.gsfc.nasa.gov/api/${typeOfImg}/date/${dateOfImg}`,
            {}
          )
            .then((response) => {
              if (!response.ok) {
                throw new Error("Not 2xx response", { cause: response });
              }
              return response.json();
            })

            .then((obj) => {
              ulmenu.textContent = undefined;
              if (obj.length === 0) {
                errorMessage(ulmenu, liTemplate);
              } else {
                createLISFromFetch(typeOfImg,dateOfImg,obj,ulmenu,liTemplate);
                ulmenu.addEventListener("click", function (e) {
                  e.stopPropagation();
                  e.preventDefault();

                  if (e.target.tagName == "SPAN") {
                    let dateArr = dateOfImg.split("-");
                    let imgIndex = e.target
                      .closest("span")
                      .getAttribute("data-index-of-my-image");
                    earthImage.src = `https://epic.gsfc.nasa.gov/archive/${typeOfImg}/${dateArr[0]}/${dateArr[1]}/${dateArr[2]}/jpg/${obj[imgIndex].image}.jpg`;
                    earthImageTitle.textContent = obj[imgIndex].caption;
                    earthImageDate.textContent = obj[imgIndex].date;
                  }
                });
              }
            })
            .catch((err) => {
              console.error("3)Error:", err);
            });
        }
      });

    //Event listener for drop down fix the dates
    document.getElementById("type").addEventListener("change", function (e) {
      setMaxDate(ulmenu, dateInput, typeInput);
    });
  });
  /**
   * Function used to take the Li's from template and decorate the span using the date and append it to the ul
   * @param {Array} obj
   * @param {HTMLElement} ulmenu
   * @param {Array} listOfImages
   * @param {HTMLElement} liTemplate
   */

  function createLIS(obj, ulmenu, liTemplate) {
    EpicApplication.listOfImages=[];
    
    for (let i = 0; i < obj.length; i++) {
      EpicApplication.listOfImages.push(obj[i].image);
      liTemplate=document.querySelector("#image-menu-item").content;
      let content=liTemplate;
      let li = content.cloneNode(true).children[0];
      li.firstChild.textContent = obj[i].date;
      li.firstChild.setAttribute("data-index-of-my-image", i);
      ulmenu.appendChild(li);
    }
  }

  
  
  
  /**
   * Function to display an error message if the length of the object array is equal to 0 meaning there is no images for a certain day.
   * @param {HTMLElement} ulmenu
   * @param {HTMLElement} liTemplate
   */
  function errorMessage(ulmenu, liTemplate) {
    let li = liTemplate.content.cloneNode(true).children[0];
    li.textContent = "Sorry No images are found for this day!";
    ulmenu.appendChild(li);
  }


  /**
   * Function used to set the max date to be able to see images for a certain image type
   * @param {HTMLElement} ulmenu
   * @param {HTMLElement} dateInput
   * @param {HTMLElement} typeInput
   */

  function setMaxDate(ulmenu, dateInput, typeInput) {
    if (EpicApplication.dateCache[typeInput.value] !== null) {
      document.getElementById("date").max =
        EpicApplication.dateCache[typeInput];
    } else {
      fetch(`https://epic.gsfc.nasa.gov/api/${typeInput.value}/all`)
        .then((response) => {
          if (!response.ok) {
            throw new Error("Not 2xx response", { cause: response });
          }
          return response.json();
        })
        .then((obj) => {
          ulmenu.textContent = undefined;
          let datesArray = obj.map((date) => new Date(date.date));
          datesArray.sort((a, b) => b - a);
          let firstDateIndex = datesArray[0];
          let formattedDate = firstDateIndex.toISOString().split("T")[0];
          dateInput.setAttribute("max", formattedDate);
          dateInput.value = formattedDate;
          EpicApplication.dateCache[typeInput.value] = formattedDate;
        })
        .catch((err) => {
          console.error("3)Error: ", err);
        });
    }
  }


/**
 * 
 * @param {HTMLElement} Select 
 * @param {Array} ImageTypeArray 
 * function to generate the options for type of image to be displayed based on an array
 */
  function generateTypeOptions(Select, ImageTypeArray) {
    ImageTypeArray.forEach((element) => {
      let option = document.createElement("option");
      option.setAttribute("value", element);
      option.textContent = element;
      Select.appendChild(option);
    });
  }



/**
 * 
 * @param {HTMLElement} typeOfImg 
 * @param {HTMLElement} dateOfImg 
 * @param {HTMLElement} ulmenu 
 * @param {HTMLElement} liTemplate 
 * Function to make images based off the cache instead of refetching
 */
  function displayCachedImages(typeOfImg, dateOfImg, ulmenu, liTemplate) {
    const cachedImages = EpicApplication.imageCache[typeOfImg].get(dateOfImg);
    if (cachedImages) {
      ulmenu.textContent = undefined;
      createLIS(cachedImages, ulmenu, EpicApplication.listOfImages, liTemplate);
    } else {
      errorMessage(ulmenu, liTemplate);
    }
  }

  /**
   * 
   * @param {HTMLElement} typeOfImg 
   * @param {HTMLElement} dateOfImg 
   * @param {Array} obj 
   * @param {HTMLElement} ulmenu 
   * @param {HTMLElement} liTemplate 
   * Function to create a list based off the information from the fetch
   */
  function createLISFromFetch(typeOfImg,dateOfImg,obj,ulmenu,liTemplate){
    EpicApplication.dateCache[typeOfImg]=dateOfImg;
    EpicApplication.imageCache[typeOfImg].set(dateOfImg,obj);
    ulmenu.textContent=undefined;
    if (EpicApplication.imageCache[typeOfImg].has(dateOfImg)) {
      const cachedImages = EpicApplication.imageCache[typeOfImg].get(dateOfImg);
      EpicApplication.listOfImages = cachedImages.map((img) => img.image);
      createLIS(cachedImages, ulmenu, liTemplate);
    } else {
      createLIS(obj, ulmenu, liTemplate);
    }
  }
  
  
})();
